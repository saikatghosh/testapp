<?php

session_start();

if(!isset($_SESSION['user'])) {
	header("location: login.php");
}

$username = $_SESSION['user'];

?>

You are logged in as <?php echo $username; ?>
<br /><a href="logout.php">Logout</a>